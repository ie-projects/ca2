[33mcommit 68da3b4d1aca09a082d321ddb2e90948264827ea[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Fri Feb 14 16:33:25 2020 +0330

    Add restaurant/id

[33mcommit 33a04fa5c5589851db17b8e251446f76feb642a0[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Fri Feb 14 13:59:17 2020 +0330

    Add get_restaurants html

[33mcommit 09a67717d2b3033ea2f0ef05f5f467c0d5efceb7[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 13 18:34:12 2020 +0330

    Remove .idea

[33mcommit 599518dc2ddff204423d43cf772ecf9a9a3c448b[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 13 18:26:09 2020 +0330

    Get restaurants from external service

[33mcommit f001bc2ab8b83093c7ca88d7ed9861544277a08f[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Sun Feb 9 13:02:46 2020 +0330

    Add nikiveryTest_2

[33mcommit 0266fc3ee6b4464935db2e5fc80b74ed018a9c5a[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 18:01:19 2020 +0330

    Add unitTests for getRecommendedRestaurant & finalizeOrder

[33mcommit 76aa72d9959bcd2bba54b0c0e83712da62ecbbc4[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 16:21:12 2020 +0330

    Add getRecommendedRestaurants, RestaurantScore class, evaluateScore

[33mcommit d084b0295989307231b931c82a4aae1dce190616[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 15:11:49 2020 +0330

    Add finalizeOrder

[33mcommit b607ccaa4454b8b6d9ddb90467fb0c15bd536704[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 15:08:10 2020 +0330

    Add getCart & class order & organizeOrder

[33mcommit 64036fe4d5035225f1e11435bd074e1af512032d[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 14:58:45 2020 +0330

    Add addToCart

[33mcommit 048ef15961597e8a751e683aec9ff28c1e6dd038[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 14:54:42 2020 +0330

    Add getFood

[33mcommit 0a1e577e7e927e72414400fd9bfcff75134764b9[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 14:47:16 2020 +0330

    Add getRestaurants & getRestaurant, serializeObject, fix bug of addRestaurant

[33mcommit 00a8d6599321e1b72dbad9cf8a26709ed2f29f3c[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Thu Feb 6 13:45:04 2020 +0330

    Add command parser, main, remove menu class

[33mcommit e62c1bb0e011f77001cf005235fbd8611b051fba[m
Author: nika mosayebi <nikibeh@yahoo.com>
Date:   Wed Feb 5 13:31:36 2020 +0330

    Add pom.xml, basic classes, addRestaurant & addFood commands

package org.fn.ie;

public class UserProfile {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private int credit;

    public UserProfile(int _id, String fname, String lname, String pnumber, String mail, int cred)
    {
        id = _id;
        firstName = fname;
        lastName = lname;
        phoneNumber = pnumber;
        email = mail;
        credit = cred;
    }

    public int getId() { return id; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getPhoneNumber() { return phoneNumber; }
    public String getEmail() { return email; }
    public int getCredit() { return credit; }
    public void addCredit(int up) { credit += up; }
}

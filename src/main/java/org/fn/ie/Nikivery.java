package org.fn.ie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Nikivery {
    private ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
    private String cartRestaurant;
    private ArrayList<Food> cart = new ArrayList<Food>();
    private UserProfile userProfile = new UserProfile(1, "Nika", "Mosayebi", "555-512-3",
            "nikibeh@yahoo.com", 50000);

    private ObjectMapper mapper = new ObjectMapper();

    public UserProfile getUserProfile() { return userProfile; }

    public String getCartRestaurant() { return cartRestaurant; }

    public void getRestaurantsFromServer()
    {
        String urlString = "http://138.197.181.131:8080/restaurants";
        URL url;
        URLConnection connection;
        InputStream input;
        try {
            url = new URL(urlString);
            connection = url.openConnection();
            input = connection.getInputStream();
            Scanner scanner = new Scanner(input, "utf-8").useDelimiter("\\A");
            String resesJson = scanner.hasNext() ? scanner.next() : "";
            restaurants = mapper.readValue(resesJson, mapper.getTypeFactory().
                    constructCollectionType(List.class, Restaurant.class));
            System.out.println(restaurants);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<Order> organizeOrders()
    {
        ArrayList<Order> orders = new ArrayList<Order>();
        for(int i = 0; i < cart.size(); i++)
        {
            int foodCount = 1;
            boolean repetitive = false;
            for(int j =0; j < cart.size(); j++)
                if(cart.get(i).getName().equals(cart.get(j).getName()))
                {
                    if(i == j)
                        continue;
                    if(j < i)
                    {
                        repetitive = true;
                        break;
                    }
                    foodCount++;
                }
            if(!repetitive)
                orders.add(new Order(cart.get(i).getName(), foodCount, cart.get(i).getPrice()));
        }
        return orders;
    }

    public void addRestaurant(String serialRestaurantInfo)
    {
        try {
            Restaurant newRestaurant = mapper.readValue(serialRestaurantInfo, Restaurant.class);
            for(int i = 0; i < newRestaurant.getMenu().size(); i++)
                newRestaurant.getMenu().get(i).setRestaurantName(newRestaurant.getName());
            restaurants.add(newRestaurant);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public ArrayList<Restaurant> getRestaurants()
    {
        ArrayList<Restaurant> validRestaurants = new ArrayList<Restaurant>();
        for(Restaurant restaurant : restaurants)
        {
            if(Math.hypot(restaurant.getLocation().getX(), restaurant.getLocation().getY()) <= 170)
                validRestaurants.add(restaurant);
        }
        return validRestaurants;
    }

    public Restaurant getRestaurant(String id)
    {
        for(Restaurant restaurant : restaurants)
        {
            if(restaurant.getId().equals(id))
            {
                return restaurant;
            }
        }
        return null;
    }

    public int addToCart(String foodName, String restaurantId)
    {
        for(Restaurant restaurant : restaurants)
        {
            if(restaurant.getId().equals(restaurantId))
            {
                for(Food food : restaurant.getMenu())
                {
                    if(food.getName().equals(foodName))
                    {
                        if(cart.size() == 0 || cartRestaurant.equals(restaurantId))
                        {
                            cart.add(food);
                            cartRestaurant = restaurantId;
                            return 0;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }
        }
        return -2;
    }

    public ArrayList<Order> getCart()
    {
        ArrayList<Order> orders = organizeOrders();
        return orders;
    }

    public int finalizeOrder()
    {
        if(cart.size() == 0)
            return -1;
        int netCost = 0;
        ArrayList<Order> orders = getCart();
        for(int i = 0; i < orders.size(); i++)
            netCost += orders.get(i).getPrice() * orders.get(i).getCount();
        if(userProfile.getCredit() < netCost)
            return -2;
        userProfile.addCredit(-1 * netCost);
        cart.clear();
        return 0;
    }

}
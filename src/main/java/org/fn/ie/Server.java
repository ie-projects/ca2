package org.fn.ie;

import io.javalin.Javalin;

import java.util.ArrayList;
import java.util.Scanner;

public class Server {

    public Nikivery nikivery = new Nikivery();

    public void configure()
    {
        nikivery.getRestaurantsFromServer();
        Javalin app = Javalin.create().start(7001);
        app.get("/", ctx -> ctx.result("Welcome to Nikivery!"));
        app.get("/get_restaurants", ctx -> {
            String response = "";
            response += "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Restaurants</title>\n" +
                    "    <style>\n" +
                    "        table {\n" +
                    "            text-align: center;\n" +
                    "            margin: auto;\n" +
                    "        }\n" +
                    "        th, td {\n" +
                    "            padding: 5px;\n" +
                    "            text-align: center;\n" +
                    "        }\n" +
                    "        .logo{\n" +
                    "            width: 100px;\n" +
                    "            height: 100px;\n" +
                    "        }\n" +
                    "    </style>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <table>\n" +
                    "        <tr>\n" +
                    "            <th>id</th>\n" +
                    "            <th>logo</th>\n" +
                    "            <th>name</th>\n" +
                    "        </tr>";

            ArrayList<Restaurant> rests = nikivery.getRestaurants();
            for(int i = 0; i < rests.size(); i++)
            {
                response += "<tr>\n" +
                        "            <td>" + rests.get(i).getId() + "</td>\n" +
                        "            <td><img class=\"logo\" src=\"" + rests.get(i).getLogo() + "\" alt=\"logo\"></td>\n" +
                        "            <td>" + rests.get(i).getName() + "</td>\n" +
                        "        </tr>";
            }
                response += "</table>\n" +
                        "</body>\n" +
                        "</html>";
            ctx.html(response);
        });
        app.get("/view_profile", ctx -> {
            String response = "";
            response += "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>User</title>\n" +
                    "    <style>\n" +
                    "        li {\n" +
                    "        \tpadding: 5px\n" +
                    "        }\n" +
                    "    </style>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <ul>\n" +
                    "        <li>id: " + nikivery.getUserProfile().getId() + "</li>\n" +
                    "        <li>full name: " + nikivery.getUserProfile().getFirstName() + " "
                    + nikivery.getUserProfile().getLastName() + "</li>\n" +
                    "        <li>phone number: " + nikivery.getUserProfile().getPhoneNumber() + "</li>\n" +
                    "        <li>email: " + nikivery.getUserProfile().getEmail() + "</li>\n" +
                    "        <li>credit: " + nikivery.getUserProfile().getCredit() + " Toman</li>\n" +
                    "        <form action=\"/credit_up\" method=\"POST\">\n" +
                    "            <button  type=\"submit\">increase</button>\n" +
                    "            <input type=\"text\" name=\"credit\" value=\"\" />\n" +
                    "        </form>\n" +
                    "    </ul>\n" +
                    "</body>\n" +
                    "</html>";
            ctx.html(response);
        });
        app.get("/view_cart", ctx -> {
            ArrayList<Order> cart = nikivery.getCart();
            if(cart.size() == 0)
                ctx.html("You currently have no food in cart!");
            else
            {
                String response = "";
                response += "<!DOCTYPE html>\n" +
                        "<html lang=\"en\">\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <title>User</title>\n" +
                        "    <style>\n" +
                        "        li, div, form {\n" +
                        "        \tpadding: 5px\n" +
                        "        }\n" +
                        "    </style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "    <div>" + nikivery.getRestaurant(nikivery.getCartRestaurant()).getName() + "</div>\n" +
                        "    <ul>\n";
                for(int i = 0; i < cart.size(); i++)
                    response += "        <li>" + cart.get(i).getFoodName() + ":" + cart.get(i).getCount() + "</li>\n";
                response += "    </ul>\n" +
                        "    <form action=\"/finalize\" method=\"POST\">\n" +
                        "        <button type=\"submit\">finalize</button>\n" +
                        "    </form>\n" +
                        "</body>\n" +
                        "</html>";
                ctx.html(response);
            }
        });
        app.post("/credit_up", ctx -> {
            System.out.println(ctx.formParam("credit"));
            String up = ctx.formParam("credit");
            try {
                nikivery.getUserProfile().addCredit(Integer.parseInt(up));
                ctx.html("Transaction completed!");
            }
            catch (NumberFormatException e)
            {
                ctx.html("Format invalid!");
            }
        });
        app.post("/finalize", ctx -> {
            int status = nikivery.finalizeOrder();
            if(status == -1)
            {
                ctx.status(400);
                ctx.html("Your cart is empty!");
            }
            else if (status == -2)
            {
                ctx.status(400);
                ctx.html("You don't have enough credit!");
            }
            else
                ctx.html("Finalized orders succesfully!");
        });
        app.post("/add_food/:res_id/:food_name", ctx -> {
            String resId = ctx.pathParam("res_id");
            String foodName = ctx.pathParam("food_name");
            int status = nikivery.addToCart(foodName, resId);
            if(status == -1)
                ctx.html("You currently have food from other restaurants in your cart!");
            else if(status == -2)
                ctx.html("Facing troubles finding the restaurant!");
            else if(status == 0)
                ctx.html("Food added to cart successfully!");
        });
        app.get("/restaurants/:id", ctx -> {
            String id = ctx.pathParam("id");
            System.out.println(id);
            Restaurant res = nikivery.getRestaurant(id);
            if(res == null)
            {
                ctx.status(404);
                String response = "";
                response += "<!DOCTYPE html>\n" +
                        "<html lang=\"en\">\n" +
                        "<body>\n" +
                        "404\n Restaurant not found.\n" +
                        "</body>\n" +
                        "</html>";
                ctx.html(response);
            }
            else if(Math.hypot(res.getLocation().getX(), res.getLocation().getY()) > 170)
            {
                ctx.status(403);
                String response = "";
                response += "<!DOCTYPE html>\n" +
                        "<html lang=\"en\">\n" +
                        "<body>\n" +
                        "403 Restaurant not found.\n" +
                        "</body>\n" +
                        "</html>";
                ctx.html(response);
            }
            else {
                ctx.status(200);
                String response = "";
                response += "<!DOCTYPE html>\n" +
                        "<html lang=\"en\">\n" +
                        "<head>\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "    <title>Restaurant</title>\n" +
                        "    <style>\n" +
                        "        img {\n" +
                        "        \twidth: 50px;\n" +
                        "        \theight: 50px;\n" +
                        "        }\n" +
                        "        li {\n" +
                        "            display: flex;\n" +
                        "            flex-direction: row;\n" +
                        "        \tpadding: 0 0 5px;\n" +
                        "        }\n" +
                        "        div, form {\n" +
                        "            padding: 0 5px\n" +
                        "        }\n" +
                        "    </style>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "    <ul>\n";
                response += "<li>id: " + res.getId() + "</li>\n" +
                        "        <li>name: " + res.getName() + "</li>\n" +
                        "        <li>location: (" + res.getLocation().getX() + ", " +
                        res.getLocation().getY() + ")</li>\n" +
                        "        <li>logo: <img src=\"" + res.getLogo() + "\" alt=\"logo\"></li>\n" +
                        "        <li>menu: \n" +
                        "        \t<ul>\n";
                ArrayList<Food> resMenu = res.getMenu();
                for (int i = 0; i < resMenu.size(); i++)
                {
                    response += "        \t\t<li>\n" +
                            "                    <img src=\"" + resMenu.get(i).getImage() + "\" alt=\"logo\">\n" +
                            "                    <div>" + resMenu.get(i).getName() + "</div>\n" +
                            "                    <div>" + resMenu.get(i).getPrice() + " Toman</div>\n" +
                            "                    <form action=\"/add_food/" + res.getId() + "/" +
                            resMenu.get(i).getName() + "\" method=\"POST\">\n" +
                            "                        <button type=\"submit\">addToCart</button>\n" +
                            "                    </form>\n" +
                            "                </li>";
                }
                response += "</ul>\n" +
                        "        </li>\n" +
                        "    </ul>\n" +
                        "</body>\n" +
                        "</html>";
                ctx.html(response);
            }
        });
    }
}
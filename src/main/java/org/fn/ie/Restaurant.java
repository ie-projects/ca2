package org.fn.ie;

import java.util.ArrayList;

public class Restaurant {
    private String id;
    private String name;
    private Location location;
    private String logo;
    private ArrayList<Food> menu;

    public void addFood(Food newFood)
    {
        menu.add(newFood);
    }

    public String getName() { return name; }

    public ArrayList<Food> getMenu() { return menu; }

    public Location getLocation() { return location; }

    public String getId() { return id; }

    public String getLogo() { return logo; }
}

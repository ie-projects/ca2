package org.fn.ie;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Test1 {

    private Server server;
    private Restaurant testRestaurant;

    @Before
    public void setup()
    {
        server = new Server();

        server.nikivery.addRestaurant(
                "{\"id\": \"nika1378\", \"name\": \"ResA\", \"location\":" +
                        " {\"x\": 10, \"y\": 13}, \"menu\": [{\"name\": \"Gheime\", \"description\": \"it’s yummy!\"," +
                        " \"popularity\": 0.8, \"price\": 10000}, {\"name\": \"Kabab\"," +
                        " \"description\": \"it’s delicious!\", \"popularity\": 0.6, \"price\": 30000}]}"
        );

        testRestaurant = server.nikivery.getRestaurant("nika1378");
        server.nikivery.getUserProfile().addCredit(200000);
        ArrayList<Food> foods = testRestaurant.getMenu();
        for(int i = 0; i < foods.size(); i++)
            server.nikivery.addToCart(foods.get(i).getName(), testRestaurant.getId());
    }

    @Test
    public void test_getRestaurant() {
        assertEquals( "nika1378", testRestaurant.getId());
        assertEquals("ResA", testRestaurant.getName());
        assertEquals(2, testRestaurant.getMenu().size());
    }

    @Test
    public void test_finalize() {
        assertEquals( 2, server.nikivery.getCart().size());
        int status = server.nikivery.finalizeOrder();
        assertEquals(0, status);
        assertEquals( 0, server.nikivery.getCart().size());
        assertEquals(210000, server.nikivery.getUserProfile().getCredit());
    }

    @After
    public void tear_down() {
        server = null;
        testRestaurant = null;
    }

}
